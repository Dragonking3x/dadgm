package com.vgdragon.dadgm.saveState;

import java.util.ArrayList;
import java.util.List;

public class Room {

    private String name;

    private List<Character> characterList;
    private List<Item> itemList;

    public void copy(Room room){
        this.name = room.getName();

        List<Character> characterList = new ArrayList<>();
        for(Character character: room.getCharacterList()){
            Character tempCharacter = new Character();
            tempCharacter.copy(character);
            characterList.add(tempCharacter);
        }
        this.characterList = characterList;

        List<Item> itemList = new ArrayList<>();
        for(Item item: room.getItemList()){
            Item tempItem = new Item();
            tempItem.copy(item);
            itemList.add(tempItem);
        }
        this.itemList = itemList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Character> getCharacterList() {
        if(characterList == null){
            return new ArrayList<>();
        }
        return characterList;
    }

    public void setCharacterList(List<Character> characterList) {
        this.characterList = characterList;
    }

    public List<Item> getItemList() {

        if(itemList == null){
            return new ArrayList<>();
        }
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }
}
