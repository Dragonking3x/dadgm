package com.vgdragon.dadgm.saveState;

import java.util.ArrayList;
import java.util.List;

public class Savestate {

    private List<Room> roomList;

    public void copy(Savestate savestate){

        List<Room> itemList = new ArrayList<>();
        for(Room room: savestate.getRoomList()){
            Room tempRoom = new Room();
            tempRoom.copy(room);
            itemList.add(tempRoom);
        }
        this.roomList = itemList;
    }

    public List<Room> getRoomList() {
        if(roomList == null){
            return new ArrayList<>();
        }

        return roomList;
    }

    public void setRoomList(List<Room> roomList) {
        this.roomList = roomList;
    }
}
