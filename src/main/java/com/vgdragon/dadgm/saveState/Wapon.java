package com.vgdragon.dadgm.saveState;

public class Wapon {

    private String name;

    private String attackBonus;
    private String critical;
    private String type;
    private String range;
    private String ammution;
    private String dmg;

    private String node;

    public void copy(Wapon wapon){
        this.name = wapon.getName();
        this.attackBonus = wapon.getAttackBonus();
        this.critical = wapon.getCritical();
        this.type = wapon.getType();
        this.range = wapon.getRange();
        this.ammution = wapon.getAmmution();
        this.dmg = wapon.getDmg();
        this.node = wapon.getNode();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAttackBonus() {
        return attackBonus;
    }

    public void setAttackBonus(String attackBonus) {
        this.attackBonus = attackBonus;
    }

    public String getCritical() {
        return critical;
    }

    public void setCritical(String critical) {
        this.critical = critical;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getAmmution() {
        return ammution;
    }

    public void setAmmution(String ammution) {
        this.ammution = ammution;
    }

    public String getDmg() {
        return dmg;
    }

    public void setDmg(String dmg) {
        this.dmg = dmg;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }
}
