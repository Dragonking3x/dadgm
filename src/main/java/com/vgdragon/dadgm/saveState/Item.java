package com.vgdragon.dadgm.saveState;

public class Item {

    private String name;

    private String number;
    private String node;

    public void copy(Item item){
        this.name = item.getName();
        this.number = item.getNumber();
        this.node = item.getNode();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNode() {
        return node;
    }

    public void setNode(String node) {
        this.node = node;
    }
}
