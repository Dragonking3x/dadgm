package com.vgdragon.dadgm.saveState;

import java.util.ArrayList;
import java.util.List;

public class Character {

    private String name;

    private String maxHitPoints;
    private String hitPoints;
    private String def;

    private String str;
    private String dex;
    private String con;
    private String intel;
    private String wis;
    private String cha;

    private String initiative;
    private String reflex;
    private String will;

    private List<Wapon> waponList;
    private List<Item> itemList;


    public void copy(Character character){
        this.name = character.getName();
        this.maxHitPoints = character.getMaxHitPoints();
        this.hitPoints = character.getHitPoints();
        this.def = character.getDef();
        this.str = character.getStr();
        this.dex = character.getDex();
        this.con = character.getCon();
        this.intel = character.getIntel();
        this.wis = character.getWis();
        this.cha = character.getCha();
        this.initiative = character.getInitiative();
        this.reflex = character.getReflex();
        this.will = character.getWill();

        List<Wapon> waponList = new ArrayList<>();
        for(Wapon wapon: character.getWaponList()){
            Wapon tempWapon = new Wapon();
            tempWapon.copy(wapon);
            waponList.add(tempWapon);
        }

        this.waponList = waponList;

        List<Item> itemList = new ArrayList<>();
        for(Item item: character.getItemList()){
            Item tempItem = new Item();
            tempItem.copy(item);
            itemList.add(tempItem);
        }
        this.itemList = itemList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMaxHitPoints() {
        return maxHitPoints;
    }

    public void setMaxHitPoints(String maxHitPoints) {
        this.maxHitPoints = maxHitPoints;
    }

    public String getHitPoints() {
        return hitPoints;
    }

    public void setHitPoints(String hitPoints) {
        this.hitPoints = hitPoints;
    }

    public String getDef() {
        return def;
    }

    public void setDef(String def) {
        this.def = def;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public String getDex() {
        return dex;
    }

    public void setDex(String dex) {
        this.dex = dex;
    }

    public String getCon() {
        return con;
    }

    public void setCon(String con) {
        this.con = con;
    }

    public String getIntel() {
        return intel;
    }

    public void setIntel(String intel) {
        this.intel = intel;
    }

    public String getWis() {
        return wis;
    }

    public void setWis(String wis) {
        this.wis = wis;
    }

    public String getCha() {
        return cha;
    }

    public void setCha(String cha) {
        this.cha = cha;
    }

    public String getInitiative() {
        return initiative;
    }

    public void setInitiative(String initiative) {
        this.initiative = initiative;
    }

    public String getReflex() {
        return reflex;
    }

    public void setReflex(String reflex) {
        this.reflex = reflex;
    }

    public String getWill() {
        return will;
    }

    public void setWill(String will) {
        this.will = will;
    }

    public List<Wapon> getWaponList() {
        if(waponList == null){
            return new ArrayList<>();
        }
        return waponList;
    }

    public void setWaponList(List<Wapon> waponList) {
        this.waponList = waponList;
    }

    public List<Item> getItemList() {
        if(itemList == null){
            return new ArrayList<>();
        }
        return itemList;
    }

    public void setItemList(List<Item> itemList) {
        this.itemList = itemList;
    }
}
