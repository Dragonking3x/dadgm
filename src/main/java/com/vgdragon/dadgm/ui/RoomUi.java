package com.vgdragon.dadgm.ui;

import com.vgdragon.dadgm.saveState.*;
import com.vgdragon.dadgm.saveState.Character;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class RoomUi {
    private JPanel jPanel;
    private JList characterJlist;
    private JButton characterAddButton;
    private JButton characterDeleteButton;
    private JButton characterOpenButton;
    private JList itemJlist;
    private JButton itemAddButton;
    private JButton itemDeleteButton;
    private JButton itemOpenButton;
    private JLabel roomNameJLabel;
    private JTextField roomTextField;
    private JButton renameRoomButton;
    private JButton addFromCharacterListButton;
    private JButton addFromItemListButton;


    private static MainUi mainUi;
    private RoomUi roomUi = this;

    private static Room currentRoom;
    private static JFrame jFrame;

    private static ListUi listUi;



    public static void roomUi(Savestate tempSavestate, int roomInt, MainUi tempMainUi){

        currentRoom = tempSavestate.getRoomList().get(roomInt);
        mainUi = tempMainUi;

        jFrame = new JFrame("Room: " + currentRoom.getName());
        jFrame.setContentPane(new RoomUi().jPanel);
        jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        jFrame.setVisible(true);
        jFrame.pack();
    }


    public RoomUi(){
        loadItemInUi();
        loadCharacterInUi();

        renameRoomButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                currentRoom.setName(roomTextField.getText());
                roomNameJLabel.setText(roomTextField.getText());
                jFrame.setTitle("Room: " + roomTextField.getText());
                mainUi.loadRoomListInUI();


            }
        });


        characterAddButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Character character = new Character();
                List<Character> characterList = currentRoom.getCharacterList();
                if(characterList == null){
                    characterList = new ArrayList<>();
                }
                character.setName(Integer.toString(characterList.size() + 1));
                characterList.add(character);
                currentRoom.setCharacterList(characterList);

                loadCharacterInUi();
            }
        });
        characterDeleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Character> characterList = currentRoom.getCharacterList();
                characterList.remove(characterJlist.getSelectedIndex());
                currentRoom.setCharacterList(characterList);

                loadCharacterInUi();
            }
        });
        characterOpenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread t = new Thread(){
                    public void run(){
                        Thread t = new Thread(){
                            public void run(){
                                CharacterUi.characterUi(currentRoom, mainUi, roomUi, characterJlist.getSelectedIndex());
                            }
                        };
                        t.start();

                    }
                };
                t.start();
            }
        });
        addFromCharacterListButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread t = new Thread() {
                    public void run() {


                        ListUi.listUi("Wapon List", roomUi, mainUi, 0);




                    }
                };
                t.start();
            }
        });



        itemAddButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Item item = new Item();
                List<Item> itemList = currentRoom.getItemList();
                if(itemList == null){
                    itemList = new ArrayList<>();
                }
                item.setName(Integer.toString(itemList.size() + 1));
                itemList.add(item);
                currentRoom.setItemList(itemList);

                loadItemInUi();
            }
        });
        itemDeleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Item> itemList = currentRoom.getItemList();
                itemList.remove(itemJlist.getSelectedIndex());
                currentRoom.setItemList(itemList);

                loadItemInUi();
            }
        });
        itemOpenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread t = new Thread(){
                    public void run(){
                        ItemUi.itemUi(currentRoom, mainUi, roomUi, itemJlist.getSelectedIndex());

                    }
                };
                t.start();
            }
        });
        addFromItemListButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread t = new Thread() {
                    public void run() {


                        ListUi.listUi("Wapon List", roomUi, mainUi, 2);




                    }
                };
                t.start();
            }
        });






    }


    public void loadCharacterInUi(){
        DefaultListModel model = new DefaultListModel();
        for(Character character: currentRoom.getCharacterList()){
            model.addElement(character.getName());
        }

        characterJlist.setModel(model);
    }
    public void loadItemInUi(){
        DefaultListModel model = new DefaultListModel();
        for(Item item: currentRoom.getItemList()){
            model.addElement(item.getName());
        }

        itemJlist.setModel(model);
    }

    public static void setListUi(ListUi listUi) {
        RoomUi.listUi = listUi;
    }

    public static Room getCurrentRoom() {
        return currentRoom;
    }
}
