package com.vgdragon.dadgm.ui;

import com.vgdragon.dadgm.saveState.Character;
import com.vgdragon.dadgm.saveState.Item;
import com.vgdragon.dadgm.saveState.Room;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class ItemUi {


    private JPanel jPanel;
    private JLabel itamNameJlabel;
    private JTextField renameTextField;
    private JButton renameButton;
    private JTextField countTextField;
    private JTextArea nodeTextArea;
    private JButton saveButton;
    private JButton saveAndCloseButton;

    // 0 = character
    // 1 = room
    private static int setting;

    private static MainUi mainUi;
    private static RoomUi roomUi;
    private static CharacterUi characterUi;
    private static WaponUi waponUi;
    private ItemUi itemUi = this;

    private static Item currentItem;
    private static JFrame jFrame;

    public static void itemUi(Character character, MainUi tempMainUi, RoomUi tempRoomUi, CharacterUi tempCharacterUi, int tempItemInt){

        setting = 0;
        mainUi = tempMainUi;
        roomUi = tempRoomUi;
        characterUi = tempCharacterUi;
        currentItem = character.getItemList().get(tempItemInt);

        jFrame = new JFrame("Item: " + currentItem.getName());
        jFrame.setContentPane(new ItemUi().jPanel);
        jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        jFrame.setVisible(true);
        jFrame.pack();

    }

    public static void itemUi(Room room, MainUi tempMainUi, RoomUi tempRoomUi, int tempItemInt){

        setting = 1;
        mainUi = tempMainUi;
        roomUi = tempRoomUi;
        currentItem = room.getItemList().get(tempItemInt);

        jFrame = new JFrame("Item: " + currentItem.getName());
        jFrame.setContentPane(new ItemUi().jPanel);
        jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        jFrame.setVisible(true);
        jFrame.pack();

    }



    public ItemUi(){
        loadItemInUi();

        renameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                currentItem.setName(renameTextField.getText());
                itamNameJlabel.setText(renameTextField.getText());
                jFrame.setTitle("Wapon: " + renameTextField.getText());

                if(setting == 0){
                    characterUi.loadItemInUi();
                } else if(setting == 1){
                    roomUi.loadItemInUi();
                }

            }
        });

        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveItemFromUi();
            }
        });
        saveAndCloseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveItemFromUi();
                jFrame.dispatchEvent(new WindowEvent(jFrame, WindowEvent.WINDOW_CLOSING));
            }
        });

    }


    public void loadItemInUi(){
        itamNameJlabel.setText(currentItem.getName());
        countTextField.setText(currentItem.getNumber());
        nodeTextArea.setText(currentItem.getNode());
    }
    public void saveItemFromUi(){
        currentItem.setName(itamNameJlabel.getText());
        currentItem.setNumber(countTextField.getText());
        currentItem.setNode(nodeTextArea.getText());
    }


}
