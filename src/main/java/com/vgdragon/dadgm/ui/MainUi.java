package com.vgdragon.dadgm.ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vgdragon.dadgm.saveState.*;
import com.vgdragon.dadgm.saveState.Character;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainUi {

    private JPanel jPanel;
    private JButton loadButton;
    private JButton saveButton;
    private JList roomJlist;
    private JButton addRoomButton;
    private JButton deleteRoomButton;
    private JButton openRoomButton;

    private Savestate savestate = new Savestate();
    private MainUi mainUi = this;





    public static void mainUi(){
        JFrame frame = new JFrame("D&D GM");
        frame.setContentPane(new MainUi().jPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.pack();
    }

    public MainUi(){
        loadRoomListInUI();

        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser jFileChooser = new JFileChooser();
                jFileChooser.setCurrentDirectory(new File(""));
                jFileChooser.setDialogTitle("Load File");
                jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

                if(jFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
                    try {
                        savestate = new ObjectMapper().readValue(new File(String.valueOf(jFileChooser.getSelectedFile())), Savestate.class);
                        loadRoomListInUI();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser jFileChooser = new JFileChooser();
                jFileChooser.setCurrentDirectory(new File(""));
                jFileChooser.setDialogTitle("Save File");
                jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                jFileChooser.setApproveButtonText("Save");

                if(jFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
                    try {
                        String fileString = String.valueOf(jFileChooser.getSelectedFile());
                        if(!fileString.endsWith(".json"))
                            fileString += ".json";

                        new ObjectMapper().writeValue(new File(fileString), savestate);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        addRoomButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Room room = new Room();
                List<Room> roomList = savestate.getRoomList();
                if(roomList == null){
                    roomList = new ArrayList<>();
                }
                room.setName(Integer.toString(roomList.size() + 1));
                roomList.add(room);
                savestate.setRoomList(roomList);

                loadRoomListInUI();
            }
        });
        deleteRoomButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Room> roomList = savestate.getRoomList();
                roomList.remove(roomJlist.getSelectedIndex());
                savestate.setRoomList(roomList);

                loadRoomListInUI();
            }
        });

        openRoomButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread t = new Thread(){
                    public void run(){
                        RoomUi.roomUi(savestate, roomJlist.getAnchorSelectionIndex(), mainUi);
                    }
                };
                t.start();
            }
        });


    }

    public void loadRoomListInUI(){
        DefaultListModel model = new DefaultListModel();
        for(Room room: savestate.getRoomList()){
            model.addElement(room.getName());
        }

        roomJlist.setModel(model);
    }

    public List<Character> getCharacterList(){
        List<Character> characterList = new ArrayList<>();
        for(Room room: savestate.getRoomList()){
            characterList.addAll(room.getCharacterList());
        }

        return characterList;
    }
    public List<Wapon> getWaponList(){
        List<Wapon> waponList = new ArrayList<>();
        for(Room room: savestate.getRoomList()){
            for(Character character: room.getCharacterList()){
                waponList.addAll(character.getWaponList());
            }
        }
        return waponList;
    }
    public List<Item> getItemList(){
        List<Item> itemList = new ArrayList<>();
        for(Room room: savestate.getRoomList()){
            itemList.addAll(room.getItemList());
            for(Character character: room.getCharacterList()){
                itemList.addAll(character.getItemList());
            }
        }
        return itemList;
    }


}
