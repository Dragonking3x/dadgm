package com.vgdragon.dadgm.ui;

import com.vgdragon.dadgm.saveState.Character;
import com.vgdragon.dadgm.saveState.Wapon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

public class WaponUi {
    private JPanel jPanel;
    private JLabel waponNameJlabel;
    private JTextField nameTextField;
    private JButton renameButton;
    private JTextField critivalTextField;
    private JTextField attackBonusTextField;
    private JTextField typeTextField;
    private JTextField rangeTextField;
    private JTextField ammunitionTextField;
    private JTextField dmgTextField;
    private JLabel attackBonusJlabel;
    private JLabel critivalJlabel;
    private JLabel typeJlabel;
    private JLabel rangeJlabel;
    private JLabel ammunitionJlabel;
    private JLabel dmgJlabel;
    private JTextArea nodeTextArea;
    private JButton saveButton;
    private JButton saveAndCloseButton;


    private static MainUi mainUi;
    private static RoomUi roomUi;
    private static CharacterUi characterUi;
    private WaponUi waponUi = this;

    private static Wapon currentWapon;
    private static JFrame jFrame;

    public static void waponUi(Character character, MainUi tempMainUi, RoomUi tempRoomUi, CharacterUi tempCharacterUi, int tempWaponInt){

        mainUi = tempMainUi;
        roomUi = tempRoomUi;
        characterUi = tempCharacterUi;
        currentWapon = character.getWaponList().get(tempWaponInt);

        jFrame = new JFrame("Wapon: " + currentWapon.getName());
        jFrame.setContentPane(new WaponUi().jPanel);
        jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        jFrame.setVisible(true);
        jFrame.pack();

    }



    public WaponUi(){

        loadWaponInUi();


        renameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                currentWapon.setName(nameTextField.getText());
                waponNameJlabel.setText(nameTextField.getText());
                jFrame.setTitle("Wapon: " + nameTextField.getText());
                characterUi.loadWaponInUi();

            }
        });

        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveWaponFromUI();
            }
        });
        saveAndCloseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveWaponFromUI();
                jFrame.dispatchEvent(new WindowEvent(jFrame, WindowEvent.WINDOW_CLOSING));
            }
        });


    }

    public void loadWaponInUi(){

        nameTextField.setText(currentWapon.getName());
        attackBonusTextField.setText(currentWapon.getAttackBonus());
        critivalTextField.setText(currentWapon.getCritical());
        typeTextField.setText(currentWapon.getType());
        rangeTextField.setText(currentWapon.getRange());
        ammunitionTextField.setText(currentWapon.getAmmution());
        dmgTextField.setText(currentWapon.getDmg());
        nodeTextArea.setText(currentWapon.getNode());

    }
    public void saveWaponFromUI(){

        currentWapon.setName(waponNameJlabel.getText());
        currentWapon.setAttackBonus(attackBonusTextField.getText());
        currentWapon.setCritical(critivalTextField.getText());
        currentWapon.setType(typeTextField.getText());
        currentWapon.setRange(rangeTextField.getText());
        currentWapon.setAmmution(ammunitionTextField.getText());
        currentWapon.setDmg(dmgTextField.getText());
        currentWapon.setNode(nodeTextArea.getText());

    }



}
