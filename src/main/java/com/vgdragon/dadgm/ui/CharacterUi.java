package com.vgdragon.dadgm.ui;

import com.vgdragon.dadgm.saveState.Character;
import com.vgdragon.dadgm.saveState.Item;
import com.vgdragon.dadgm.saveState.Room;
import com.vgdragon.dadgm.saveState.Wapon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class CharacterUi {
    private JPanel jPanel;
    private JTextField CharacterNameTextField;
    private JButton CharacterNameButton;
    private JLabel CharacterNameJLabel;
    private JList waponJlist;
    private JButton waponAddButton;
    private JButton waponRemoveButton;
    private JButton waponOpenButton;
    private JList itemJlist;
    private JButton itemAddButton;
    private JButton itemDeleteButton;
    private JButton itemOpenButton;
    private JButton saveButton;
    private JButton saveAndCloseButton;
    private JTextField hitpointsMaxTextField;
    private JTextField initiativeTextField;
    private JTextField defensiveTextField;
    private JTextField strTextField;
    private JTextField conTextField;
    private JTextField wisTextField;
    private JTextField hitpointsTextField;
    private JTextField reflexTextField;
    private JTextField willTextField;
    private JTextField dexTextField;
    private JTextField intTextField;
    private JTextField chaTextField;
    private JButton addFromItemListButton;
    private JButton addFromWaponListButton;

    private static MainUi mainUi;
    private static RoomUi roomUi;
    private CharacterUi characterUi = this;

    private static Character currentCharacter;
    private static JFrame jFrame;
    private static ListUi listUi;



    public static void characterUi(Room tempRoom, MainUi tempMainUi, RoomUi tempRoomUi, int tempCharacterInt) {

        mainUi = tempMainUi;
        roomUi = tempRoomUi;
        currentCharacter = tempRoom.getCharacterList().get(tempCharacterInt);

        jFrame = new JFrame("Character: " + currentCharacter.getName());
        jFrame.setContentPane(new CharacterUi().jPanel);
        jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        jFrame.setVisible(true);
        jFrame.pack();


    }

    public CharacterUi() {

        loadItemInUi();
        loadWaponInUi();
        loadCharacterInUi();

        CharacterNameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                currentCharacter.setName(CharacterNameTextField.getText());
                CharacterNameJLabel.setText(CharacterNameTextField.getText());
                jFrame.setTitle("Character: " + CharacterNameTextField.getText());
                roomUi.loadCharacterInUi();


            }
        });


        waponAddButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Wapon wapon = new Wapon();
                List<Wapon> waponList = currentCharacter.getWaponList();
                if (waponList == null) {
                    waponList = new ArrayList<>();
                }
                wapon.setName(Integer.toString(waponList.size() + 1));
                waponList.add(wapon);
                currentCharacter.setWaponList(waponList);

                loadWaponInUi();
            }
        });
        waponRemoveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Wapon> waponList = currentCharacter.getWaponList();
                waponList.remove(waponJlist.getSelectedIndex());
                currentCharacter.setWaponList(waponList);

                loadWaponInUi();
            }
        });
        waponOpenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread t = new Thread() {
                    public void run() {
                        WaponUi.waponUi(currentCharacter, mainUi, roomUi, characterUi, waponJlist.getSelectedIndex());

                    }
                };
                t.start();
            }
        });


        itemAddButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Item item = new Item();
                List<Item> itemList = currentCharacter.getItemList();
                if (itemList == null) {
                    itemList = new ArrayList<>();
                }
                item.setName(Integer.toString(itemList.size() + 1));
                itemList.add(item);
                currentCharacter.setItemList(itemList);

                loadItemInUi();
            }
        });
        itemDeleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Item> itemList = currentCharacter.getItemList();
                itemList.remove(itemJlist.getSelectedIndex());
                currentCharacter.setItemList(itemList);

                loadItemInUi();
            }
        });
        itemOpenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread t = new Thread() {
                    public void run() {

                        ItemUi.itemUi(currentCharacter, mainUi, roomUi, characterUi, itemJlist.getSelectedIndex());

                    }
                };
                t.start();
            }
        });
        addFromItemListButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Thread t = new Thread() {
                    public void run() {


                        ListUi.listUi("Item List", characterUi, mainUi, 2);




                    }
                };
                t.start();

            }
        });

        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveCharacterFromUi();
            }
        });
        saveAndCloseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveCharacterFromUi();
                jFrame.dispatchEvent(new WindowEvent(jFrame, WindowEvent.WINDOW_CLOSING));
            }
        });
        addFromWaponListButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Thread t = new Thread() {
                    public void run() {


                        ListUi.listUi("Wapon List", characterUi, mainUi, 1);



                    }
                };
                t.start();
            }
        });


    }


    public void loadItemInUi() {
        DefaultListModel model = new DefaultListModel();
        for (Item item : currentCharacter.getItemList()) {
            model.addElement(item.getName());
        }

        itemJlist.setModel(model);
    }

    public void loadWaponInUi() {
        DefaultListModel model = new DefaultListModel();
        for (Wapon wapon : currentCharacter.getWaponList()) {
            model.addElement(wapon.getName());
        }

        waponJlist.setModel(model);
    }

    public void loadCharacterInUi() {
        CharacterNameTextField.setText(currentCharacter.getName());
        hitpointsMaxTextField.setText(currentCharacter.getMaxHitPoints());
        initiativeTextField.setText(currentCharacter.getInitiative());
        defensiveTextField.setText(currentCharacter.getDef());
        strTextField.setText(currentCharacter.getStr());
        conTextField.setText(currentCharacter.getCon());
        wisTextField.setText(currentCharacter.getWis());
        hitpointsTextField.setText(currentCharacter.getHitPoints());
        reflexTextField.setText(currentCharacter.getReflex());
        willTextField.setText(currentCharacter.getWill());
        dexTextField.setText(currentCharacter.getDex());
        intTextField.setText(currentCharacter.getIntel());
        chaTextField.setText(currentCharacter.getCha());

    }

    public void saveCharacterFromUi() {
        currentCharacter.setName(CharacterNameTextField.getText());
        currentCharacter.setMaxHitPoints(hitpointsMaxTextField.getText());
        currentCharacter.setInitiative(initiativeTextField.getText());
        currentCharacter.setDef(defensiveTextField.getText());
        currentCharacter.setStr(strTextField.getText());
        currentCharacter.setCon(conTextField.getText());
        currentCharacter.setWis(wisTextField.getText());
        currentCharacter.setHitPoints(hitpointsTextField.getText());
        currentCharacter.setReflex(reflexTextField.getText());
        currentCharacter.setWill(willTextField.getText());
        currentCharacter.setDex(dexTextField.getText());
        currentCharacter.setIntel(intTextField.getText());
        currentCharacter.setCha(chaTextField.getText());
    }


    public static Character getCurrentCharacter() {
        return currentCharacter;
    }
}
