package com.vgdragon.dadgm.ui;

import com.vgdragon.dadgm.saveState.Character;
import com.vgdragon.dadgm.saveState.Item;
import com.vgdragon.dadgm.saveState.Room;
import com.vgdragon.dadgm.saveState.Wapon;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

public class ListUi {
    private JPanel jPanel;
    private JLabel titelJLabel;
    private JList listJlist;
    private JButton addButton;

    private static List<Object> objectList;

    // 0 = Charater
    // 1 = Wapon
    // 2 = Item
    //
    private static int listType;

    // 0 = Character
    // 1 = Room
    // 2 = main
    private static int fromClass;

    private static List<Character> characterList;
    private static List<Wapon> waponList;
    private static List<Item> itemList;



    private static JFrame jFrame;
    private ListUi listUiClass = this;

    private static CharacterUi characterUi;
    private static RoomUi roomUi;
    private static MainUi mainUi;

    private Item item;
    private Wapon wapon;
    private Character character;






    public static void listUi(String titleText){

        jFrame = new JFrame(titleText);
        jFrame.setContentPane(new ListUi().jPanel);
        jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        jFrame.setVisible(true);
        jFrame.pack();
    }

    public static void listUi(String titleText, CharacterUi tempCharacterUi, MainUi tempMainUi, int tempListType){
        listType = tempListType;
        mainUi = tempMainUi;
        characterUi = tempCharacterUi;

        fromClass = 0;
        listUi(titleText);
    }
    public static void listUi(String titleText, RoomUi tempRoomUi, MainUi tempMainUi, int tempListType){
        listType = tempListType;
        mainUi = tempMainUi;
        roomUi = tempRoomUi;
        fromClass = 1;
        listUi(titleText);

    }
    public static void listUi(String titleText, MainUi tempMainUi, int tempListType){
        listType = tempListType;
        mainUi = tempMainUi;
        fromClass = 2;
        listUi(titleText);
    }



    public ListUi(){
        DefaultListModel model = new DefaultListModel();
        if(listType == 0){

            characterList = mainUi.getCharacterList();
            for(Character tempCharacter: characterList){
                model.addElement(tempCharacter.getName());
            }

        } else if(listType == 1){
            waponList = mainUi.getWaponList();
            for(Wapon tempWapon: waponList){
                model.addElement(tempWapon.getName());
            }
        } else if(listType == 2){
            itemList = mainUi.getItemList();
            for(Item tempItem: itemList){
                model.addElement(tempItem.getName());
            }
        }
        listJlist.setModel(model);


        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(listJlist.getSelectedIndex() >= 0){
                    closeing();
                }
            }
        });



    }

    private void closeing(){
        if(fromClass == 0){
            if(listType == 0) {

            } else if(listType == 1){
                Wapon wapon = waponList.get(listJlist.getSelectedIndex());
                if (wapon != null) {
                    Character currentCharacter = characterUi.getCurrentCharacter();
                    List<Wapon> waponList = currentCharacter.getWaponList();


                    Wapon tempWapon = new Wapon();
                    tempWapon.copy(wapon);

                    waponList.add(tempWapon);
                    currentCharacter.setWaponList(waponList);

                    characterUi.loadWaponInUi();
                }


            } else if(listType == 2){
                Item item = itemList.get(listJlist.getSelectedIndex());
                if (item != null) {
                    Character currentCharacter = characterUi.getCurrentCharacter();
                    List<Item> itemList = currentCharacter.getItemList();

                    Item tempItem = new Item();
                    tempItem.copy(item);

                    itemList.add(tempItem);
                    currentCharacter.setItemList(itemList);

                    characterUi.loadItemInUi();
                }

            }

        } else if(fromClass == 1){
            if(listType == 0) {

                Character character = characterList.get(listJlist.getSelectedIndex());
                if (character != null) {
                    Room currentRoom = roomUi.getCurrentRoom();
                    List<Character> characterList = currentRoom.getCharacterList();


                    Character tempCharacter = new Character();
                    tempCharacter.copy(character);

                    characterList.add(tempCharacter);
                    currentRoom.setCharacterList(characterList);

                    roomUi.loadCharacterInUi();
                }

            } else if(listType == 1){
                //characterUi.setAddWapon(waponList.get(listJlist.getSelectedIndex()));

            } else if(listType == 2){
                Item item = itemList.get(listJlist.getSelectedIndex());
                if (item != null) {
                    Room currentRoom = roomUi.getCurrentRoom();
                    List<Item> itemList = currentRoom.getItemList();
                    if (itemList == null) {
                        itemList = new ArrayList<>();
                    }
                    Item tempItem = new Item();
                    tempItem.copy(item);

                    itemList.add(tempItem);
                    currentRoom.setItemList(itemList);

                    roomUi.loadItemInUi();
                }
            }

        } else if(fromClass == 2){

        }
        jFrame.dispatchEvent(new WindowEvent(jFrame, WindowEvent.WINDOW_CLOSING));
    }

}
